const AWS = require('aws-sdk');
const DDB = require('./sharedDynamo.js');
const SQS = new AWS.SQS();
const SNS = new AWS.SNS();

exports.handler = async(event) => {
   if (event.hasOwnProperty('Records')) {
      //sqs event
      for (const record of event.Records) { await this.processMessage(JSON.parse(record.body)); }
   }
   else {
      // normal lambda invoke
      await this.processMessage(event);
   }
};


exports.processMessage = async(event) => {
   switch (event.messageType) {
      case 'setPlayback':
         return await this.setPlayback(event);
      case 'tagDataRaw':
         return await this.startPlayback(event);
      default:
         console.log('unknown message: %j', event);
   }
};



exports.setPlayback = async function(event) {

   //query dynamo records within date ranges
   var data = await DDB.getReplayData(event);

   console.log("Found %s items", data.Items.length);

   event.LastEvaluatedKey = data.LastEvaluatedKey;

   if (event.LastEvaluatedKey) {
      //spawn another lambda
      console.log('spawning additional lambda');
      await this.spawnLambda(event);
   }
   else {
      console.log('Done Searching');
   }

   //loop through items
   for (let i = 0; i < data.Items.length; i++) {

      //set item to be republished
      //await DDB.flagForReplay(data.Items[i]);

      //emulate live stream message
      var message = {
         messageType: "tagDataRaw",
         gatewayId: data.Items[i].gatewayId,
         receivedAt: data.Items[i].receivedAt,
         messageBody: data.Items[i].messageBody
      };

      //send to SQS
      await this.sendSqs(JSON.stringify(message));

   }

};


exports.startPlayback = async function(event) {
   //check if LIVE processing queue is low enough to inject a playback record

   let depth = await this.getQueueDepth();
   do {
      //wait for LIVE queue to low enough to fill
      if (depth > process.env.THRESHOLD) {
         setTimeout(() => {
            console.log('queue level too high: %s, waiting to drain.', depth);
         }, 5000);
         depth = await this.getQueueDepth();
      }
   } while (depth > process.env.THRESHOLD);

   //send SNS
   console.log('Republishing message from gateway %s at %s', event.gatewayId, event.receivedAt);
   await this.sendSns(JSON.stringify(event));

};


//spawn another lambda like this one
exports.spawnLambda = async function(event) {
   var lambda = new AWS.Lambda();
   return lambda.invoke({
      InvocationType: 'Event',
      FunctionName: 'arn:aws:lambda:us-east-1:321679960309:function:iot-oem-laird-republish',
      Payload: JSON.stringify(event)
   }).promise();
};



//getQueueDepth of live stream
exports.getQueueDepth = async function() {
   return new Promise(
      function(resolve, reject) {
         var params = {
            QueueUrl: process.env.SQS_URL_LIVE,
            /* required */
            AttributeNames: [
               'ApproximateNumberOfMessages',
               'ApproximateNumberOfMessagesNotVisible'
               /* more items */
            ]
         };

         SQS.getQueueAttributes(params, function(err, data) {
            if (err) {
               console.log('SQS error: %s', err);
               reject(err);
            }
            else {
               resolve(Number(data.Attributes.ApproximateNumberOfMessages) + Number(data.Attributes.ApproximateNumberOfMessagesNotVisible));
            }
         });
      }
   );

};


//send message to SQS
exports.sendSqs = async function(message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            "MessageBody": message,
            "QueueUrl": process.env.SQS_URL_REPUBLISH
         };

         SQS.sendMessage(params, function(err, data) {
            if (err) {
               console.log('SQS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};

//send message to SNS
exports.sendSns = async function(message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            "Message": message,
            "TopicArn": process.env.SNS_ARN
         };

         SNS.publish(params, function(err, data) {
            if (err) {
               console.log('SNS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};
/* JUNK PILE

exports.handler = async(event) => {
   
   //add some counters to the event
   if (!event.countFound) { event.countFound = 0 }
   if (!event.countSent) { event.countSent = 0 }
   if (!event.chainCount) { event.chainCount = 0 }

   var items = await this.ListObjects(event);
   event.countFound += items.Contents.length;
   //console.log('items: %j', items);

   //chain lambdas to handle large datasets
   if (items.IsTruncated && process.env.ALLOW_SPAWN == 'true' && event.chainCount <= process.env.MAX_SPAWN) {
      event.ContinuationToken = items.NextContinuationToken;

      //track to prevent infinite lambda chaining
      event.chainCount += 1;

      console.log("spawning additional lambda: %j", event);
      await this.spawnLambda(event);
   }

   //loop through keys
   for (let i = 0; i < items.Contents.length; i++) {
      var key = items.Contents[i].Key;

      //check for valid messageType
      if (key.includes("tagDataRaw")) {
         //get the file
         var item = await this.getObject(key);

         //send to parsing lambda
         //console.log('sending message: %j', key);

         //insert into Dynamo
         await this.addToDynamo(key, item);

         //await this.sendMessage(item);

         event.countSent += 1;
      }
   }

   console.log("Done: %j", event);

   return event;
};


*/
