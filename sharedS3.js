const AWS = require('aws-sdk');
const S3 = new AWS.S3();

//List objects in S3
exports.ListObjects = async function(event) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            Bucket: process.env.S3_BUCKET,
            /* required */
            MaxKeys: 1000,
            Prefix: event.path,
            ContinuationToken: event.ContinuationToken || null
         };

         S3.listObjectsV2(params, function(err, data) {
            if (err) {
               console.log('S3 error: %s', err);
               reject(err);
            }
            else {
               //return JSON object
               resolve(data);
            }
         });
      }
   );
};


//Get object from S3
exports.getObject = async function(file) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            Bucket: process.env.S3_BUCKET,
            Key: file
         };

         S3.getObject(params, function(err, data) {
            if (err) {
               console.log('S3 error: %s', err);
               reject(err);
            }
            else {
               //return JSON object
               resolve(data.Body.toString('utf-8'));
            }
         });
      }
   );
};



//JUNK PILE
/* 
//Save to S3
exports.saveToS3 = async function(key, item) {
   try {

      //get timestamp from key
      //2020/12/01/gateways/354616090297099/oem/tagDataRaw/SECONDS.json

      //make new key
      //Laird/MG100/tagDataRaw/2021/01/07/deviceId-354616090297099/
      console.log('old key: %s', key)
      
      var path = key.split('/')
      var time = path[7].replace('.','');
      var new_key = 'Laird/MG100/tagDataRaw/' + path[0] + '/' + path[1] + '/' + path[2] + '/deviceId-' + path[4] + '/' + time
      console.log('new key: %s', new_key)

      //write data to bucket
      let params = {
         Bucket: 'pom-iot-data', //root bucket
         Key: new_key, //file name/key
         Body: item, //json to upload,
         ACL: 'bucket-owner-full-control', //access policy,
         ContentType: 'application/json'
      };

      return await S3.putObject(params).promise();

   }
   // request failed
   catch (ex) {
      console.error(ex);
   }
};
*/
