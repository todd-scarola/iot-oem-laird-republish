const AWS = require('aws-sdk');
const DDB = new AWS.DynamoDB.DocumentClient();


exports.getReplayData = async(event) => {
   var params = {
      TableName: process.env.TABLE_GATEWAY_DATA,
      FilterExpression: "#receivedAt BETWEEN :eventStart AND :eventEnd",
      ExpressionAttributeNames: {
         "#receivedAt": "receivedAt"
      },
      ExpressionAttributeValues: {
         ":eventStart": Number(event.eventStart),
         ":eventEnd": Number(event.eventEnd)
      },
      ProjectionExpression: "gatewayId, receivedAt, messageBody"
   };

   if (event.LastEvaluatedKey) {
      params.ExclusiveStartKey = event.LastEvaluatedKey;
   }

   return await DDB.scan(params).promise();
};


exports.flagForReplay = async(item) => {
   try {
      var params = {
         TableName: process.env.TABLE_GATEWAY_DATA,
         Key: {
            "gatewayId": item.gatewayId,
            "receivedAt": item.receivedAt
         },
         UpdateExpression: "SET republish = :republish",
         ExpressionAttributeValues: {
            ":republish": true
         },
         ReturnValues: "UPDATED_NEW"
      };

      return await DDB.update(params).promise();
   }
   catch (err) {
      console.log('ERROR processing setReplayData message: %j', err);
   }

};

exports.setReplayDataComplete = async(item) => {
   try {
      var params = {
         TableName: process.env.TABLE_GATEWAY_DATA,
         Key: {
            "gatewayId": item.gatewayId,
            "receivedAt": item.receivedAt
         },
         UpdateExpression: "SET republish = :republish",
         ExpressionAttributeValues: {
            //DELETE properties here

            //DANGER
         },
         ReturnValues: "ALL_NEW"
      };

      return await DDB.update(params).promise();
   }
   catch (err) {
      console.log('ERROR processing setReplayDataComplete message: %j', err);
   }

};




exports.addToDynamo = async(key, item) => {
   try {

      //extract gatewayId from key
      //Laird/MG100/tagDataRaw/2021/01/07/deviceId-354616090297099/
      var gatewayId = key.split('/')[6].substring(9);

      //extract timestamp
      //Laird/MG100/tagDataRaw/2021/01/07/deviceId-354616090297099/SECONDS.json
      var timestamp = key.split('/')[7].replace('.json', '');


      var params = {
         TableName: 'iot-gateway-data',
         Key: {
            gatewayId: { S: gatewayId },
            receivedAt: { N: timestamp.toString() }
         },
         UpdateExpression: 'SET messageBody=:messageBody, messageType=:messageType, topic=:topic, dataSource=:dataSource',
         ExpressionAttributeValues: {
            ":messageBody": { "S": JSON.parse(item).messageBody },
            ":messageType": { "S": 'tagDataRaw' },
            ":topic": { "S": 'mg100/gw/' + gatewayId + '/up' },
            ":dataSource": { "S": 'republished' }
         },
         ReturnValues: "ALL_NEW"
      };

      console.log('key: %s, dynamo: %j', key, params);

      //return await DDB.updateItem(params).promise();
   }
   catch (err) {
      console.log('ERROR processing addToDynamo: %j, %j', err, key);
   }
};
